# grab-bucket #
Simple script for get a local copy of all personal BitBucket repos

## Usage ##
```
grab-bucket.py bb_login bb_password dest_dir
```
Where:

* __bb_login__ - BitBucket login
* __bb_password__ - BitBucket password
* __dest_dir__ - destination directory

### Notes ###
* Git and Mercurial repositories supported
* All repos cloned without source files (only a repository's data)
* If the destination directory already contains a repository from BitBucket, it just updated
* If the destination directory contains a repository that is not in BitBucket, it will be renamed to 'repo_name.deleted'

### Requirements ###

* [Python](http://www.python.org/download/) (tested on python 2.7.5)
* [Git](http://git-scm.com/) (if used)
* [Mercurial](http://mercurial.selenic.com/) (if used)
