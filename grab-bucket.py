#!/usr/bin/env python
# coding: utf-8
###############################################################################
# @copyright Copyright (c) 2013 Boris Ulyanov
#
# @brief Simple script for get a local copy of all personal BitBucket repos
###############################################################################

import os
import sys
import base64
import urllib2
import json
import subprocess


DEL_MARK = '.deleted'


class VCSExecutor:
    def __init__(self, user, password, root_dir):
        self.user = user
        self.password = password
        self.root_dir = root_dir

    def run(self, command, sub_dir=None):
        if sub_dir:
            proc_dir = os.path.join(self.root_dir, sub_dir)
        else:
            proc_dir = self.root_dir
        pipe = subprocess.Popen(command, cwd=proc_dir, universal_newlines=True)
        rc = pipe.wait()
        return(rc)

    def make_url(self, name, vcs_type):
        url = 'https://' + self.user + ':' + self.password + \
              '@bitbucket.org/' + self.user + '/' + name
        if vcs_type == u'git':
            url += '.git'
        return(url)

    def clone(self, name, vcs_type):
        url = self.make_url(name, vcs_type)
        if vcs_type == u'git':
            self.run(['git', 'clone', '--no-checkout', url])
        elif vcs_type == u'hg':
            self.run(['hg', 'clone', '--noupdate', url])
        else:
            sys.stderr.write('Unexpected repo type - [%s] for [%s]\n'
                             % (vcs_type, name))

    def fetch(self, name, vcs_type):
        url = self.make_url(name, vcs_type)
        if vcs_type == u'git':
            self.run(['git', 'fetch', url], name)
        elif vcs_type == u'hg':
            self.run(['hg', 'pull', url], name)
        else:
            sys.stderr.write('Unexpected repo type - [%s] for [%s]\n'
                             % (vcs_type, name))


def scan_dir(root_dir):
    root_dir = os.path.abspath(root_dir)
    if not os.path.isdir(root_dir):
        sys.exit('Directory [' + root_dir + '] not exist\n')

    encode = sys.getfilesystemencoding() or 'utf-8'
    print 'Scan [%s] for existing repos' % root_dir
    rv = []

    for dir_item in os.listdir(root_dir):
        dir_item_full = os.path.join(root_dir, dir_item)

        if not os.path.isdir(dir_item_full):
            sys.stderr.write('Unexpected file - [%s]\n' % dir_item_full)
            continue

        if dir_item.endswith(DEL_MARK):
            continue

        dot_git_dir = os.path.join(dir_item_full, '.git')
        dot_hg_dir = os.path.join(dir_item_full, '.hg')

        vcs = None
        if os.path.isdir(dot_git_dir):
            vcs = u'git'
        elif os.path.isdir(dot_hg_dir):
            vcs = u'hg'
        else:
            sys.stderr.write('Unexpected not VSC directory - [%s]\n' %
                             dir_item_full)
            continue
        if vcs:
            rv.append((dir_item.decode(encode), vcs))
    return rv


def bb_make_func_full_name2name(username):
    start_idx = len(username) + 1

    def name_func(full_repo_name):
        return full_repo_name[start_idx:]
    return name_func


def bb_get_repo_list(username, password):
    rv = []
    request_url = 'https://bitbucket.org/api/2.0/repositories/%s' % username
    credentials = base64.encodestring("%s:%s" % (username, password))[:-1]
    headers = {'Authorization': "Basic " + credentials}
    func_full_name2name = bb_make_func_full_name2name(username)

    while (True):
        print 'Request ', request_url
        try:
            request = urllib2.Request(url=request_url, headers=headers)
            handler = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            print e.headers
            print e
            sys.exit('Response error')
        except urllib2.URLError, e:
            print e.reason
            sys.exit('Access error')

        page_data = json.load(handler)
        # the key [name] may be different from the url :( =>
        # extract from [full_name]
        page_repos = [(func_full_name2name(r['full_name']), r['scm'])
                      for r in page_data['values']]
        rv.extend(page_repos)
        if (u'next' not in page_data):
            return rv
        request_url = page_data[u'next']


def main(user, password, root_dir):
    print 'Grab all BitBucket repos for [%s] in [%s]' % (user, root_dir)

    bb_repos = bb_get_repo_list(user, password)
    if not bb_repos:
        sys.exit('Empty repo list from BB')

    present_repos = scan_dir(root_dir)

    vcs = VCSExecutor(user, password, root_dir)

    for (bb_name, bb_type) in bb_repos:
        if (bb_name, bb_type) in present_repos:
            vcs.fetch(bb_name, bb_type)
            present_repos.remove((bb_name, bb_type))
        else:
            vcs.clone(bb_name, bb_type)

    for (present_name, present_type) in present_repos:
        print 'Mark [%s] as [%s]' % (present_name, DEL_MARK)
        src_name = os.path.join(root_dir, present_name)
        dst_name = src_name + DEL_MARK
        try:
            os.rename(src_name, dst_name)
        except OSError:
            sys.stderr.write('Error rename [%s] to [%s]\n' % (src_name, DEL_MARK))

    print 'Done!'


def usage():
    print
    print 'usage: grab-bucket.py [login] [passwd] [dest_dir]\n'
    print


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) is not 3:
        usage()
        sys.exit(2)

    main(user=args[0], password=args[1], root_dir=os.path.abspath(args[2]))
